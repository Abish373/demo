import { useEffect, useState } from "react";

function App() {
  return (
    <div>
      <Users />
    </div>
  );
}

export function Users() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`https://api.github.com/users`)
      .then((res) => res.json())
      .then((data) => setUsers(data));
  }, []);

  return (
    <div>
      {users.map((el) => (
        <div key={el.login}>{el.login}</div>
      ))}
    </div>
  );
}

export default App;
